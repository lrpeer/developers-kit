require 'rouge' unless defined? ::Rouge.version

module Rouge; module Themes
  class Custom < CSSTheme
    name 'alpinebits'

    style Comment,           fg: '#333333', italic: true
    style Str,               fg: '#000000'
    style Str::Char,         fg: '#800080'
    style Num,               fg: '#0000ff'
    style Keyword,           fg: '#000080', bold: true
    style Operator::Word,    bold: true
    style Name::Tag,         fg: '#ff0000', bold: true
    style Name::Attribute,   fg: '#600000', bold: true
    style Generic::Deleted,  fg: '#000000', bg: '#ffdddd', inline_block: true, extend: true
    style Generic::Inserted, fg: '#000000', bg: '#ddffdd', inline_block: true, extend: true
    style Text, {}
  end
end; end
