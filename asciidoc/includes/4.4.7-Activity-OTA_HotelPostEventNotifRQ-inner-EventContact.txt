<EventContact Role="chief">
    <PersonName>
        <GivenName>Werner</GivenName>
        <Surname>Call</Surname>
    </PersonName>

    <URL Type="Image">https://example.com/image.jpg</URL>
    <EmployeeInfo EmployeeId="Chief cook"/>
</EventContact>