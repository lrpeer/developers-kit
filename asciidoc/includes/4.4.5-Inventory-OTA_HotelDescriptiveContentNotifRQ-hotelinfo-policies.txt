<!-- Policies: check-in and check_out period, min guest age, ... -->
<Policies>
	<Policy>
		<!-- Check-in and Check-out range -->
		<StayRequirements>
			<StayRequirement StayContext="Checkin" Start="15:00:00" End="20:00:00"/>
			<StayRequirement StayContext="Checkout" Start="06:00:00" End="10:00:00"/>
		</StayRequirements>
	</Policy>
</Policies>
