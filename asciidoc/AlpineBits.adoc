:toc:
:toclevels: 3
:source-highlighter: rouge
:rouge-style: alpinebits


// Variables
:AlpineBitsR: pass:q[AlpineBits^(R)^]
:AlpineBitsLastRelease: 2018-10
// EOF Variables


ifdef::official-release[]
:AlpineBitsVersion: {ReleaseVersion}
:front-cover-image: image::{coverpage}[]
endif::[]

ifndef::official-release[]
:AlpineBitsVersion: Unreleased unofficial Draft
:title-logo-image: image::includes/logo-ccbysa.png[alt=CC-BY-SA logo,pdfwidth=127mm,align=center]
= pass:normal[{AlpineBitsR} {AlpineBitsVersion}]: Based on {AlpineBitsLastRelease}
AlpineBits is a registered trademark of the AlpineBits Alliance - https://www.alpinebits.org


{AlpineBitsR} is an interface specification for exchanging data in the tourism sector, specially tailored for alpine tourism.

The interface is based on XML messages that validate against version 2015A of the OpenTravel Schema by the OpenTravel Alliance.

© AlpineBits Alliance. This document is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License <<anchor-footnote0,^0^>>.

Permissions beyond the scope of this license may be available at https://www.alpinebits.org.
endif::[]

<<<
include::00-preamble.adoc[]

<<<
include::00-changelog.adoc[]

<<<
include::01-introduction.adoc[]

<<<

include::02-https_request_response.adoc[]

<<<
include::03-handshaking.adoc[]

<<<
include::04-data_exchange.adoc[]

<<<
include::0A-server_response.adoc[]

<<<
include::0B-developer_resources.adoc[]

<<<
include::0C-version_compatibility.adoc[]

<<<
include::0D-external_links.adoc[]
